{ pkgs }: {
  deps = [
    pkgs.sqlite
    (pkgs.python3.withPackages
      (ps: [ ps.flask ps.feedparser ps.bleach ps.uvicorn ]))
  ];
}
