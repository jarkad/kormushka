{
  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";
  inputs.devshell.url = "github:numtide/devshell";
  inputs.devshell.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, devshell }:
    let
      eachSystem = nixpkgs.lib.genAttrs [ "x86_64-linux" "aarch64-linux" ];
      pkgsFor = system:
        import nixpkgs {
          inherit system;
          overlays = [ devshell.overlay ];
        };
      pydepsFor = ps: [ ps.flask ps.feedparser ps.bleach ps.uvicorn ];
      depsFor = pkgs: rec {
        pythonNoDeps = pkgs.python3;
        python = pythonNoDeps.withPackages pydepsFor;
      };
    in {
      devShell = eachSystem (system:
        let
          pkgs = pkgsFor system;
          deps = depsFor pkgs;
        in pkgs.devshell.mkShell {
          imports = [ (pkgs.devshell.importTOML ./devshell.toml) ];
          packages = [ deps.python ];
        });

      packages = eachSystem (system:
        let
          pkgs = pkgsFor system;
          deps = depsFor pkgs;
        in {
          kormushka = pkgs.python3Packages.buildPythonPackage rec {
            name = "kormushka";
            version = "1.0";
            src = pkgs.nix-gitignore.gitignoreSourcePure [
              ".envrc"
              ".fslckout"
              "__FOSSIL__"
              "devshell.toml"
              "flake.lock"
              "flake.nix"
              "replit.nix"
              ./.fossil-settings/ignore-glob
            ] ./.;
            propagatedBuildInputs = pydepsFor deps.pythonNoDeps.passthru.pkgs;
          };

          standalone = pkgs.writeShellScriptBin "kormushka" ''
            ${
              deps.pythonNoDeps.withPackages
              (ps: pydepsFor ps ++ [ self.packages.${system}.kormushka ])
            }/bin/uvicorn kormushka:create_app --factory --interface wsgi "$@"
          '';

          docker-image = pkgs.dockerTools.buildLayeredImage {
            name = "kormushka";
            tag = "latest";
            extraCommands = "mkdir -p data";
            contents = [ self.packages.${system}.standalone ];
            config = {
              Env = [ "PORT=5000" ];
              WorkingDir = "/data";
              Entrypoint = [ "/bin/kormushka" ];
            };
          };

          load-docker = pkgs.writeShellScriptBin "load-docker" ''
            set -euo pipefail
            docker load -i ${self.packages.${system}.docker-image}
          '';

          deploy = pkgs.writeShellScriptBin "heroku-deploy" ''
            set -euo pipefail
            docker load -i ${self.packages.${system}.docker-image}
            docker tag kormushka registry.heroku.com/kormushka/web
            heroku container:login
            docker push registry.heroku.com/kormushka/web
            heroku container:release web -a kormushka
          '';

          dev = pkgs.writeShellScriptBin "dev" ''
            set -euo pipefail
            ${deps.python}/bin/uvicorn --reload --factory --interface wsgi kormushka:create_app
          '';

          default = self.packages.${system}.kormushka;
        });
    };
}
