from base64 import b64decode
from datetime import datetime
from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from kormushka.db import get_db
from kormushka.db import to_obj
from time import mktime
from werkzeug.exceptions import abort
import feedparser
import sqlite3

FEEDS_PER_PAGE = 10
POSTS_PER_PAGE = 10
THE_DAY_IT_ALL_BEGUN = datetime.fromtimestamp(1).timetuple()

bp = Blueprint('main', __name__)


# Utilities


def is_hx():
    return 'HX-Request' in request.headers


def tpl(name):
    if is_hx():
        return f'main/partial/{name}.html'
    return f'main/{name}.html'


def tt2dt(timetuple):
    time = mktime(timetuple)
    return datetime.fromtimestamp(time)


def guess_title(feed, rss=None):
    if feed.title and feed.title.strip():
        title = feed.title
    elif rss and 'feed' in rss and 'title' in rss['feed']:
        title = rss['feed']['title']
    else:
        title = ''

    title = title.strip()

    if title == '':
        title = None

    return title


def refresh_feed(feed, rss=None):
    db = get_db()
    rss = rss or feedparser.parse(feed.url)
    updated = datetime.now()
    title = guess_title(feed, rss)

    feed = to_obj(db.execute(
        'UPDATE feed'
        ' SET title = ?, updated = ?'
        ' WHERE id IS ?'
        ' RETURNING *',
        (title, updated, feed.id),
    ))

    if feed is not None:
        db.executemany(
            'INSERT OR REPLACE INTO post'
            ' (id, feed_id, published, title, author, summary, link)'
            ' VALUES (?, ?, ?, ?, ?, ?, ?)',
            [(
                post.get('id', post.get('link', post.get('title', None))),
                feed.id,
                tt2dt(post.get('published_parsed', THE_DAY_IT_ALL_BEGUN)),
                post.get('title', None),
                post.get('author', None),
                post.get('summary', None),
                post.get('link', None)
            ) for post in rss.get('entries', [])],
        )

    return feed


def refresh_all_feeds():
    feeds = to_obj(get_db().execute(
        'SELECT *'
        ' FROM feed',
    ), multi=True)

    for feed in feeds:
        refresh_feed(feed)


# Routes


@bp.route('/posts', defaults={'page': -1})
@bp.route('/posts/page/<int:page>')
def posts(page):
    if page == -1:
        page = datetime.now().timestamp()

    page = datetime.fromtimestamp(page)

    db = get_db()
    posts = to_obj(db.execute(
        'SELECT * FROM post'
        ' WHERE published < ?'
        ' ORDER BY published DESC'
        ' LIMIT ?',
        (page, POSTS_PER_PAGE),
    ), multi=True)

    next_page = None
    if len(posts) > 0:
        last_post_timestamp = (
            datetime
            .fromisoformat(posts[-1].published)
            .timestamp()
        )
        next_page = url_for('.posts', page=last_post_timestamp)

    return render_template(tpl('posts'),
                           posts=posts,
                           next_page=next_page)


@bp.route('/posts/refresh', methods=('GET', 'POST'))
def posts_refresh():
    refresh_all_feeds()
    get_db().commit()

    return redirect(url_for('.posts'))


@bp.route('/posts/<id>', methods=['GET'])
def post(id):
    db = get_db()

    id = str(b64decode(id), 'utf-8')

    post = to_obj(
        db.execute(
            'SELECT post.id, feed_id, published, post.title, author, summary, link, feed.title AS feed_title'
            ' FROM post'
            ' JOIN feed'
            ' ON post.feed_id IS feed.id'
            ' WHERE post.id IS ?',
            (id,),
        ),
        fields=[
            'id',
            'feed_id',
            'published',
            'title',
            'author',
            'summary',
            'link',
            'feed_title',
        ])

    if post is None:
        abort(404)

    return render_template(tpl('post'), post=post)


@bp.route('/feeds', methods=('GET',), defaults={'page': -1})
@bp.route('/feeds/page/<int:page>', methods=('GET',))
def feeds(page):
    '''Show all the feeds page by page.'''
    feeds = to_obj(get_db().execute(
        'SELECT * FROM feed'
        ' WHERE id > ?'
        ' ORDER BY id ASC'
        ' LIMIT ?',
        (page, FEEDS_PER_PAGE),
    ), multi=True)

    next_page = None
    if len(feeds) > 0:
        next_page = url_for('.feeds',
                            page=feeds[-1].id)

    return render_template(tpl('feeds'),
                           next_page=next_page,
                           feeds=feeds)


@bp.route('/feeds/new', methods=['GET'])
def feeds_new():
    if is_hx():
        return render_template(tpl('feed'), editable=True)
    return render_template('main/feeds_new.html')


@bp.route('/feeds/<int:id>', methods=['GET'])
def feed(id):
    db = get_db()
    feed = to_obj(db.execute(
        'SELECT * FROM feed'
        ' WHERE id IS ?',
        (id,),
    ))

    if feed is None:
        abort(404)

    editable = 'editable' in request.args

    return render_template(tpl('feed'),
                           feed=feed,
                           editable=editable)


@bp.route('/feeds', methods=['POST'], defaults={'id': None})
@bp.route('/feeds/<int:id>', methods=['POST'])
def feed_upsert(id):
    if 'url' not in request.form:
        abort(400, 'URL is required')

    url = request.form['url']
    title = request.form.get('title', '').strip()

    if title == '':
        title = None

    try:
        rss = feedparser.parse(url)
    except Exception:
        abort(400, f'Invalid URL: {url}')

    db = get_db()

    if id is None:
        try:
            feed = to_obj(db.execute(
                'INSERT INTO feed'
                ' (url, title)'
                ' VALUES (?, ?)'
                ' RETURNING *',
                (url, title),
            ))
        except sqlite3.IntegrityError:
            abort(400, 'Feed already exists')
    else:
        feed = to_obj(db.execute(
            'UPDATE feed'
            ' SET url = ? , title = ?'
            ' WHERE id IS ?'
            ' RETURNING *',
            (url, title, id),
        ))

    if feed is None:
        db.rollback()
        abort(404)

    feed = refresh_feed(feed, rss)

    db.commit()

    if is_hx():
        return render_template(tpl('feed'),
                               feed=feed,
                               editable=False)

    return redirect(url_for('.feed', id=feed.id))


@bp.route('/feeds/<int:id>/delete', methods=['POST'])
def feed_delete(id):
    db = get_db()

    feed = to_obj(db.execute(
        'DELETE FROM feed'
        ' WHERE id IS ?'
        ' RETURNING *',
        (id,),
    ))

    if feed is None:
        db.rollback()
        abort(404)

    db.commit()

    # if is_hx():
    #     return ''

    return redirect(url_for('.feeds'))
