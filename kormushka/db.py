from flask import current_app
from flask import g
from flask.cli import with_appcontext
from importlib import import_module
from types import SimpleNamespace
import click


def get_db():
    '''Connect to the application's configured database. The connection
    is unique for each request and will be reused if this is called
    again.
    '''
    if 'db' not in g:
        api = import_module(current_app.config['DATABASE_MODULE'])
        g.db = api.connect(**current_app.config['DATABASE'])
    return g.db


def close_db(e=None):
    '''If this request connected to the database, close the
    connection.
    '''
    db = g.pop('db', None)

    if db is not None:
        db.close()


def init_db():
    '''Clear existing data and create new tables.'''
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))


def mock_db():
    '''Clear existing data and create new tables with test data.'''
    db = get_db()

    with current_app.open_resource('test_data.sql') as f:
        db.executescript(f.read().decode('utf8'))


@click.command('init-db')
@click.option('-t', '--with-test-data', is_flag=True)
@with_appcontext
def init_db_command(with_test_data):
    '''Clear existing data and create new tables.'''
    init_db()
    if with_test_data:
        mock_db()
    click.echo('Initialized the database.')


def init_app(app):
    '''Register database functions with the Flask app. This is called by
    the application factory.
    '''
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)


def row_to_obj(fields, row):
    if row:
        return SimpleNamespace(**dict(zip(fields, row)))


def to_obj(query, fields=None, multi=False):
    fields = fields or [d[0] for d in query.description]
    if multi:
        return [row_to_obj(fields, row) for row in query.fetchall()]
    else:
        return row_to_obj(fields, query.fetchone())
