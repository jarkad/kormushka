INSERT INTO feed (url) VALUES
  ('https://planet.haskell.org/rss20.xml'),
  ('https://planet.lisp.org/rss20.xml'),
  ('https://planet.nixos.org/atom.xml'),
  ('https://planet.scheme.org/atom.xml');
