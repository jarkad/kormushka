import os

from flask import Flask
from flask import redirect
from flask import url_for


def create_app(test_config=None):
    '''Create and configure an instance of the Flask application.'''
    app = Flask(__name__,
                instance_path=os.getcwd(),
                instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE_MODULE='sqlite3',
        DATABASE={
            'database': os.path.join(app.instance_path, 'kormushka.sqlite'),
        },
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.update(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from kormushka import db
    from kormushka import main
    from kormushka import filters

    db.init_app(app)
    app.register_blueprint(main.bp)
    app.register_blueprint(filters.bp)

    @app.route('/')
    def index():
        return redirect(url_for('main.posts'))

    return app
