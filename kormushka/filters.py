from base64 import b64encode
from base64 import b64decode
from flask import Blueprint
from markupsafe import Markup
import bleach
import re

bp = Blueprint('filters', __name__)


ALLOWED_TAGS = [
    'a',
    'abbr',
    'acronym',
    'article',
    'audio',
    'b',
    'blockquote',
    'cite',
    'code',
    'div',
    'em',
    'footer',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'header',
    'i',
    'img',
    'li',
    'mark',
    'ol',
    'p',
    'picture',
    'pre',
    'q',
    'span',
    'strong',
    'sub',
    'sup',
    'table',
    'tbody',
    'td',
    'th',
    'thead',
    'time',
    'tr',
    'ul',
    'video',
]

ALLOWED_ATTRS = {
    '*': ['id'],
    'a': ['href', 'title'],
    'abbr': ['title'],
    'acronym': ['title'],
    'img': ['alt', 'src'],
}

ALLOWED_PROTOCOLS = [
    'http',
    'https',
    'mailto',
    'tel',
    'tg',
]


@bp.app_template_filter()
def sanitize(text, strip=True):
    return Markup(bleach.clean(
        text or '',
        strip=strip,
        tags=ALLOWED_TAGS,
        attributes=ALLOWED_ATTRS,
        protocols=ALLOWED_PROTOCOLS,
    ))


@bp.app_template_filter()
def strip_html(text):
    text = bleach.clean(text, strip=True, tags=[])
    text = re.sub(r'(?m)^\s*(.+)\s*$', r'\1<br>', text)
    return Markup(text)


@bp.app_template_filter()
def enc64(text):
    return str(b64encode(bytes(text, 'utf-8')), 'utf-8')
