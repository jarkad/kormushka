-- Initialize the database.
-- Drop any existing data and create empty tables.

PRAGMA foreign_keys=ON;

DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS feed;

CREATE TABLE feed (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  title TEXT,
  url TEXT NOT NULL UNIQUE,
  updated TIMESTAMP
);

CREATE TABLE post (
  id TEXT PRIMARY KEY,
  feed_id INTEGER NOT NULL,
  published TIMESTAMP NOT NULL,
  title TEXT NOT NULL,
  author TEXT,
  summary TEXT,
  link TEXT NOT NULL,
  FOREIGN KEY (feed_id) REFERENCES feed (id) ON DELETE CASCADE
);
